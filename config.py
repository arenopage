#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
"""Configuration file for arenopage bot"""

# folders configuration
folders = { 'plugins' : './plugins', 'logs' : './logs' }
if not folders['plugins'] in sys.path: sys.path.append(folders['plugins'])
# configuration of connecting
connection_data = {
    'host' : 'irc.freenode.net',
    'port' : 6667,
    'nick' : 'arenopage',
    'ident' : 'arenopage',
    'name' : 'owner: pagenoare',
    'channels' : ['#webtips-pl'], 
    'password' : 'passwd'
}

# how plugins load on start?
plugins_auto_load = ['log']