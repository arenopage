#!/usr/bin/python
# -*- coding: utf-8 -*-
import time


class plugin(object):
    def install(self, arenopage, args):
        try:
            file_name = "%s.txt" % time.strftime("%d_%m_%Y")
            file_open = open(arenopage.folders['logs'] + "/" + file_name, "a")
            file_open.write("%s:%s:%s:%s:%s\n" % (arenopage.data['nick'], arenopage.data['host'], arenopage.data['action'], arenopage.data['to'], arenopage.data['msg']))
        finally:
            file_open.close()