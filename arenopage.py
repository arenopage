#!/usr/bin/python
# -*- coding: utf-8 -*-
import socket, re

class arenopage(object):
    def __init__(self, config):
        try:
            self.folders, self.connection_data, self.plugins_on_load = config.folders, config.connection_data, config.plugins_auto_load
        except:
            print "You have error in config file!"
    
    def connect(self):
        self.irc = socket.socket()
        connect = self.irc.connect((self.connection_data['host'], self.connection_data['port']))
        self.query("NICK %s" % self.connection_data['nick'])
        self.query("USER %s host server : %s" % (self.connection_data['ident'], self.connection_data['name']))
        if self.connection_data['password']:
            self.query("PRIVMSG NickServ :IDENTIFY %s" % self.connection_data['password'])
        for channel in self.connection_data['channels']:
            self.query("JOIN %s" % channel)
        while True:
            self.do_action(self.irc.recv(500).split('\r\n')[0])
            self.load_plugin()
    
    def query(self, query):
        self.irc.send(query + '\r\n')
        
    def do_action(self, data):
        """
        Parse data, autoload plugins.
        """
        try:
            regexp = re.compile(':(.*)!n=(.*) ([A-Z]+) :{0,1}(#{0,1}[A-Za-z0-9\-\_\.]*)\s*:{0,1}(.*)')
            check = regexp.match(data)
            self.data = {
                'nick' : check.group(1),
                'host' : check.group(2),
                'action' : check.group(3),
                'to' : check.group(4),
                'msg' : check.group(5)
            }
            print self.data
            for plugin in self.plugins_on_load:
                self.load_plugin(plugin)
        except:
            pass
    
    def load_plugin(self, plugin = None):
        self.plugins = {}
        try:
            if not plugin:
                check = re.compile("^!%s (.*)" % self.connection_data['nick']).match(self.data['msg']).group(1).split()
                plugin_data = { 'name' : check[0], 'args' : check[1:] }
            else:
                plugin_data = { 'name' : plugin, 'args' : '' }
            if not self.plugins.has_key(plugin_data['name']):
                try:
                    imported = __import__(plugin_data['name'])
                    self.plugin = imported.plugin()
                    self.plugins[plugin_data['name']] = self.plugin
                    for plugin in self.plugins:
                        getattr(self.plugins[plugin], 'install')(self, plugin_data['args'])
                except:
                    pass
        except:
            pass
    
    def send_msg(self, msg, to = None):
        if not to:
            for channel in self.connection_data['channels']:
                self.query("PRIVMSG %s :%s" % (channel, msg))
        elif isinstance(to, list):
            for receiver in to:
                self.query("PRVMSG %s :%s" % (receiver, msg))
        elif isinstance(to, str):
            self.query("PRIVMSG %s :%s" % (to, msg))
                

        
        